import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ListingsService {
  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    private http: HttpClient
  ) {}

  private _editlistners = new Subject<any>();
  editlisten(): Observable<any> {
    return this._editlistners.asObservable();
  }

  editChange(filterBy: string) {
    this._editlistners.next(filterBy);
  }

  private _createlistners = new Subject<any>();
  createlisten(): Observable<any> {
    return this._createlistners.asObservable();
  }

  createChange(filterBy: string) {
    this._createlistners.next(filterBy);
  }

  getListings(storeID) {
    return this.db.list(`Stores/${storeID}/Listings`).snapshotChanges();
  }

  getListingsData(storeID, listingsID) {
    return this.db.list(`Stores/${storeID}/Listings/${listingsID}`).snapshotChanges();
  }

  getListingsFileImage(storeID, listingsID) {
    return this.db.list(`Stores/${storeID}/Listings/${listingsID}/files`).snapshotChanges();
  }

  getListingsMainavatar(storeID, listingsID, fileID) {
    let ref = this.storage.ref(`Store/${storeID}/${listingsID}/${fileID}`);
    if (ref) {
      return ref.getDownloadURL();
    } else {
      Observable.throw(() => {
        console.log("Get listings file error!");
      });
    }
  }

  getAllCountries() {
    return this.db.list(`SystemSettings/Countries`).snapshotChanges();
  }

  getAllSettings() {
    return this.db.list(`SystemSettings/Global/services`).snapshotChanges();
  }

  // getBannerUrl() {
  //   this.http
  //     .get(
  //       "https://search.ob1.io/search/listings?q=iphone&network=mainnet&p=0&ps=24&acceptedCurrencies=BTC&condition=any&moderators=verified_mods&nsfw=false&rating=0&shipping=any&type=any",
  //       {}
  //     )
  //     .toPromise()
  //     .then(res => {
  //       return res(["results"]["results"][0]["data"]["thumbnail"]["large"]);
  //     })
  //     .catch(err => {
  //     });
  // }
}
