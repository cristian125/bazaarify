import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { UserService } from 'app/services/user.service';
import { ConfigurationService } from 'app/services/configuration/configuration.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class StoreService {
  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    private userService: UserService,
    private configurationService: ConfigurationService,
    private http: HttpClient
  ) {
  }
  
  getStoreData(storeId) {
    return this.db.object(`Stores/${storeId}`).valueChanges().subscribe(data => {
      return data;
    });
  }

  getStoreValue(storeId): Observable<any> {
    return this.db.object(`Stores/${storeId}`).valueChanges();
  }

  getStoreSnapshot(storeId): Observable<any> {
    return this.db.object(`Stores/${storeId}`).snapshotChanges();
  }

  getStoresValue(storeIdsArr): Observable<any> {
    let storesObservables = [];
    storeIdsArr.map(storeId => {
      storesObservables.push(this.getStoreSnapshot(storeId).map(
        item => {
          console.log(item);
          let storeId = item['key'];
          let storeData = {};
          storeData[storeId] = item.payload.val();
          return storeData;
        }
      ));
    });
    return Observable.combineLatest(
      storesObservables
    );
  }

  getStoreMainavatar(storeId) {
    let ref = this.storage.ref(`Store/${storeId}/storeImages/logo.jpg`);
    // console.log(ref);
    if (ref) {
      return ref.getDownloadURL()
    } else {
      Observable.throw(() => {
        console.log('error');
      })
    }
  }

  getStorePrice() {
    let configuration = this.configurationService.getConfiguration();
    return configuration['UserEnrollments']['StorePrice'];
  }

  getStoreEnrollmentPeriod() {
    let configuration = this.configurationService.getConfiguration();
    return configuration['UserEnrollments']['EnrollmentPeriod'];
  }

  getAllLanguages() {
    return this.db.object(`SystemSettings/Languages`).valueChanges();
  }

  getProvisonTemplate() {
    return this.db.object(`SystemSettings/ProvisionTemplates`).valueChanges();
  }

  browseModerators(moderators_url, auth_key) {
    return this.http
      .get(
        // "https://searchapi.bazaarify.me/ob/moderators",
        moderators_url,
        {
          headers: new HttpHeaders({
            'Authorization': auth_key,
          })
        }
      )
      .toPromise()
      .then(res => {
        return res;
      })
      .catch(err => {
        return err;
      });
  }

  getModeratorProfile(peer_url, auth_key) {
    return this.http
      .get(
        peer_url,
        {
          headers: new HttpHeaders({
            'Authorization': auth_key
          }),
        }
      )
      .toPromise()
      .then(res => {
        return res;
      })
      .catch(err => {
        return err;
      });
  }

  getModeratorsAvatar(image_url, auth_key) {
    return this.http
      .get(
        image_url,
        {
          headers: new HttpHeaders({
            'Authorization': auth_key,
          }),
          responseType: 'arraybuffer'
        }
      )
      .toPromise()
      .then(res => {        
        return res;
      })
      .catch(err => {
        return err;
      });
  }
}
