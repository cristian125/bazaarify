import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, UrlHandlingStrategy } from "@angular/router";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage";
import { HttpClient } from "@angular/common/http";
import { ListingsService } from "../../services/listings/listings.service";
import { MatDialog } from "@angular/material";
import { ListingsDeleteComponent } from "../dialogs/listings-delete/listings-delete.component";

@Component({
  selector: "app-listings-display",
  templateUrl: "./listings-display.component.html",
  styleUrls: ["./listings-display.component.scss"]
})
export class ListingsDisplayComponent implements OnInit {
  storeID: string;
  listingsID: string;
  store_name: string;
  store_logo: string;
  editorConfig = {
    editable: false,
    spellcheck: false,
    height: "150px",
    minHeight: "100px",
    placeholder: "Type something. Test the Editor... ヽ(^。^)丿",
    translate: "no"
  };
  page_no = 1;

  //Page1 UI Value
  moderators = [];
  title: string;
  upload_flag = false;
  allImages = [];
  selectedFile = null;
  price = "";
  price_unit: string;
  allCountries = [];
  condition: string;
  allConditions = [];
  contractType: string;
  allTypes = [];
  sku: string;
  tags = [];
  categories = [];
  mature_content: string = "false";
  reviews: string;

  //Page2 UI Value
  description = "";
  shipping_options = [];
  selected_option = {};
  shipping_filter: string;
  cntServices: number;
  destinations_dataName = [];
  destinations_name = [];
  filter_regions = [];
  filter_key: string;
  european_economic = [];
  european_union = [];
  destination_Settings = {
    singleSelection: false,
    idField: "item_id",
    textField: "item_text",
    enableCheckAll: false,
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 5
  };

  //Page3 UI Value
  inventory_flag = 0;
  quantity_flag = 0;
  quantity = "";
  variants = [];
  variant_inventory = [];
  return_policy = "";
  terms_condition = "";
  coupons = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    private http: HttpClient,
    private listingsService: ListingsService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.storeID = params.storeID;
      this.listingsID = params.listingsID;
    });

    this.condition = "New";
    // this.type = "Listing_post_ob_listing";
    this.price_unit = "ALL";

    this.db
      .list(`Stores/${this.storeID}/Profile/data/storeModerators`)
      .snapshotChanges()
      .subscribe(item => {
        item.forEach(element => {
          this.moderators.push(element.payload.val());
        });
      });

    this.db
      .list(`Stores/${this.storeID}`)
      .snapshotChanges()
      .subscribe(item => {
        let stores = [];
        item.forEach(element => {
          var y = element.payload.val();
          if (element.key == "Profile") {
            this.store_name = y["data"]["name"];
          }
        });
      });

    let logo = this.storage.ref(
      `Store/${this.storeID}/Profile/store_logo_tiny.jpg`
    );
    logo.getDownloadURL().subscribe(data => {
      this.store_logo = data;
    });

    this.listingsService.getAllCountries().subscribe(item => {
      let temp_dataName = [];
      let temp_name = [];
      item.forEach(element => {
        let y = element.payload.val();
        this.allCountries.push(y["code"]);
        temp_dataName.push(y["dataName"]);
        temp_name.push(y["name"]);
      });

      this.destinations_dataName = temp_dataName;
      this.destinations_name = temp_name;
      this.allCountries.sort();
    });

    this.db
      .list(`SystemSettings/ShippingDestinations`)
      .snapshotChanges()
      .subscribe(item => {
        item.forEach(element => {
          let region = {};
          let y = element.payload.val();
          if (element.key !== "ALL") {
            for (let key in y) {
              region["key"] = key;
              let temp = y[key];
              let data = [];
              temp.forEach(element => {
                let index = this.destinations_dataName.indexOf(element);
                if (index !== -1) {
                  data.push(this.destinations_name[index]);
                }
              });
              region["data"] = data;
              this.filter_regions.push(region);
            }
          } else {
            region["key"] = y;
            region["data"] = this.destinations_name;
            this.filter_regions.push(region);
          }
        });

        this.filter_key = this.filter_regions[0].key;
      });

    this.listingsService.getAllSettings().subscribe(item => {
      item.forEach(element => {
        if (element.key == "listingConditions") {
          let y: string = element.payload.val();
          this.allConditions = y.split(",");
        } else if (element.key == "listingTypes") {
          let y = element.payload.val();
          const str = JSON.stringify(y);
          JSON.parse(str, (key, value) => {
            if (key !== "") {
              this.allTypes.push(value);
            }
          });
        }
      });
    });

    this.listingsService
      .getListingsData(this.storeID, this.listingsID)
      .subscribe(item => {
        if (this.upload_flag == false) {
          item.forEach(element => {
            let y = element.payload.val();
            switch (element.key) {
              case "data":
                // Page1
                this.title = y["item"]["title"];
                this.price = y["item"]["price"];
                this.condition = y["item"]["condition"];
                let categories = y["item"]["categories"];
                this.categories = [];
                if (
                  Object.prototype.toString.call(categories) ===
                  "[object Array]"
                ) {
                  categories.forEach(element => {
                    let temp = {};
                    temp["displayValue"] = element;
                    this.categories.push(temp);
                  });
                }

                let tags = y["item"]["tags"];
                this.tags = [];
                if (Object.prototype.toString.call(tags) === "[object Array]") {
                  tags.forEach(element => {
                    let temp = {};
                    temp["displayValue"] = element;
                    this.tags.push(temp);
                  });
                }

                // Page2
                this.selected_option = {};
                this.selected_option["name"] = "";
                this.selected_option["regions"] = [];
                this.selected_option["services"] = [];
                this.selected_option["type"] = "";
                this.shipping_options = [];
                if (
                  Object.prototype.toString.call(y["shippingOptions"]) ===
                  "[object Array]"
                ) {
                  let shipping_options = y["shippingOptions"];
                  shipping_options.forEach(element => {
                    let temp = {};
                    temp["name"] = element["name"];
                    temp["type"] = element["type"];
                    temp["regions"] = [];
                    temp["services"] = [];
                    if (
                      Object.prototype.toString.call(element["regions"]) ===
                      "[object Array]"
                    ) {
                      temp["regions"] = element["regions"];
                    }
                    if (
                      Object.prototype.toString.call(element["services"]) ===
                      "[object Array]"
                    ) {
                      temp["services"] = element["services"];
                    }

                    this.shipping_options.push(temp);
                  });
                  this.selected_option = this.shipping_options[0];
                  this.shipping_filter = this.selected_option["name"];
                } else {
                  this.shipping_options.push(this.selected_option);
                  this.shipping_filter = "";
                }

                this.contractType = y["metadata"]["contractType"];
                this.price_unit = y["metadata"]["pricingCurrency"];
                this.selected_option["type"] = y["metadata"]["format"];
                this.mature_content = y["item"]["nsfw"];
                this.description = y["item"]["description"];

                // Page3
                this.quantity = y["item"]["quantity"];
                this.return_policy = y["refundPolicy"];
                this.terms_condition = y["termsAndConditions"];
                let options = y["item"]["options"];
                if (
                  Object.prototype.toString.call(options) === "[object Array]"
                ) {
                  options.forEach(element => {
                    let variant = {};
                    variant["name"] = element["name"];
                    let choices = [];
                    element["variants"].forEach(element => {
                      let temp = {};
                      temp["displayValue"] = element["name"];
                      choices.push(temp);
                    });
                    variant["choices"] = choices;
                    this.variants.push(variant);
                  });
                }

                let skus = [];
                if (
                  Object.prototype.toString.call(y["item"]["skus"]) ===
                  "[object Array]"
                ) {
                  skus = y["item"]["skus"];
                  for (let sku of skus) {
                    let inventory = [];
                    inventory["productID"] = sku["productID"];
                    inventory["quantity"] = sku["quantity"];
                    inventory["surcharge"] = sku["surcharge"];
                    inventory["variantCombo"] = sku["variantCombo"];
                    let data = [];
                    for (let i = 0; i < sku["variantCombo"].length; i++) {
                      data.push(
                        options[i]["variants"][sku["variantCombo"][i]]["name"]
                      );
                    }
                    inventory["data"] = data;
                    this.variant_inventory.push(inventory);

                    this.quantity_flag = 0;
                    if (this.variant_inventory[0]["quantity"] !== "") {
                      this.inventory_flag = 1;
                    }
                  }
                } else {
                  if (this.quantity !== "") {
                    this.quantity_flag = 1;
                    this.inventory_flag = 1;
                  }
                }

                let coupons = y["coupons"];
                this.coupons = [];
                if (
                  Object.prototype.toString.call(coupons) === "[object Array]"
                ) {
                  coupons.forEach(element => {
                    this.coupons.push(element);
                  });
                }

                break;
              case "files":
                this.allImages = [];
                y.forEach(element => {
                  let temp = {};
                  temp["name"] = element;
                  this.listingsService
                    .getListingsMainavatar(
                      this.storeID,
                      this.listingsID,
                      element
                    )
                    .subscribe(data => {
                      temp["url"] = data;
                    });

                  this.allImages.push(temp);
                });
                break;
              case "type":
                // this.type = y;
                break;
            }
          });
        } else {
          item.forEach(element => {
            let y = element.payload.val();
            if (element.key == "files") {
              this.allImages = [];
              y.forEach(element => {
                let temp = {};
                temp["name"] = element;
                this.listingsService
                  .getListingsMainavatar(this.storeID, this.listingsID, element)
                  .subscribe(data => {
                    temp["url"] = data;
                  });

                this.allImages.push(temp);
              });
            }
          });
          this.upload_flag = false;
        }
      });
  }

  previous() {
    if (this.page_no == 1) return;
    this.page_no--;
  }

  next() {
    if (this.page_no == 4) return;
    this.page_no++;
  }

  onItemSelect(event) {
    if (event == "EUROPEAN_ECONOMIC_AREA") {
      // this.selected_option['regions'] = this.european_economic;
      // console.log(this.selected_option['regions']);
      // this.european_economic.forEach(element => {
      //   this.selected_option['regions'].push(element);
      // })
    }
  }

  onItemDeselect(event) {
    if (event == "EUROPEAN_ECONOMIC_AREA") {
      // this.selected_option['regions'].splice(0, 10);
      // console.log(this.selected_option['regions']);
    }
  }

  onSelectAll(event) {}

  inputTitle(event) {
    this.shipping_filter = event.target.value;
  }

  onShippingsFilter(value) {
    this.shipping_filter = value;
    this.shipping_options.forEach(element => {
      if (element["name"] == value) {
        this.selected_option = element;
      }
    });
  }

  getRealArray(variants, n) {
    let skus = [];
    let result = [];
    if (n < 2) {
      let ret = [];
      let i = 0;
      if (
        Object.prototype.toString.call(variants[0]["data"]) ===
        "[object Undefined]"
      ) {
        for (let variant of variants[0]) {
          let sku = {};
          let temp = [];
          let no = [];
          temp.push(variant);
          no.push(i);
          sku["data"] = temp;
          sku["variantCombo"] = no;
          sku["productID"] = "";
          sku["surcharge"] = "";
          sku["quantity"] = "";
          ret.push(sku);
          i++;
        }
        return ret;
      }
      return variants;
    }

    let i = 0,
      j = 0;
    for (let temp1 of variants[n - 2]) {
      for (let temp2 of variants[n - 1]) {
        let ret = {};
        let check = [];
        let no = [];
        check.push(temp1);
        no.push(i);
        if (
          Object.prototype.toString.call(temp2["data"]) === "[object Array]"
        ) {
          for (let temp3 of temp2["data"]) check.push(temp3);
          for (let temp3 of temp2["variantCombo"]) no.push(temp3);
        } else if (
          Object.prototype.toString.call(temp2["data"]) === "[object Undefined]"
        ) {
          check.push(temp2);
          no.push(j);
        } else {
          check.push(temp2["data"]);
          no.push(temp2["variantCombo"]);
        }

        ret["data"] = check;
        ret["variantCombo"] = no;
        ret["productID"] = "";
        ret["surcharge"] = "";
        ret["quantity"] = "";
        result.push(ret);
        j++;
      }
      i++;
      j = 0;
    }

    variants.pop();
    variants.pop();
    variants.push(result);
    if (variants.length == 1) {
      skus = this.getRealArray(result, variants.length);
    } else {
      skus = this.getRealArray(variants, variants.length);
    }

    return skus;
  }

  unlimited(event, i) {
    if (event.target.checked) {
      this.variant_inventory[i]["quantity"] = "-";
    } else {
      this.variant_inventory[i]["quantity"] = "";
    }
  }

  onEditListing() {
    this.router.navigate([
      "/listings/edit/",
      this.storeID,
      this.listingsID,
      this.page_no
    ]);
  }

  onCloneListing() {
    var UUID = require("uuid-js");
    var uuid4 = UUID.create();
    let new_listingsID = uuid4.toString();
    this.router.navigate([
      "/listings/clone",
      this.storeID,
      this.listingsID,
      new_listingsID,
      this.page_no
    ]);
  }

  onDeleteListing() {
    let dialogRef = this.dialog.open(ListingsDeleteComponent, {
      data: {
        store: this.storeID,
        listings: this.listingsID,
        title: "Are you sure?",
        content: "Once it's deleted, it's gone forever.",
        mode: "delete_listing"
      },
      width: "300px"
    });
  }

  onReturnListing() {
    this.router.navigate(["/listings", this.storeID]);
  }
}
