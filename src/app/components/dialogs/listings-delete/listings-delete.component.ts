import { Component, Inject, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage";
import { ListingsService } from "../../../services/listings/listings.service";

@Component({
  selector: "app-listings-delete",
  templateUrl: "./listings-delete.component.html",
  styleUrls: ["./listings-delete.component.scss"]
})
export class ListingsDeleteComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ListingsDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private storage: AngularFireStorage,
    private db: AngularFireDatabase,
    private listingsService: ListingsService,
    private router: Router
  ) {}

  ngOnInit() {}

  delete() {
    let mode = this.data["mode"];
    let storeID = this.data["store"];
    let listingsID = this.data["listings"];

    let images = [];
    this.listingsService
      .getListingsData(storeID, listingsID)
      .subscribe(item => {
        item.forEach(element => {
          if (element.key == "files") {
            images = element.payload.val();
            images.forEach(element => {
              let ref = this.storage.ref(`Store/${storeID}/${listingsID}`);
              ref.child(element).delete();
            });
          }
        });
      });

    let listings = {};
    this.db
      .object(`Stores/${storeID}/Listings/${listingsID}`)
      .set(listings)
      .then(function() {
        console.log("Success");
      })
      .catch(function(error) {
        console.log("Error: " + error);
      });

    this.router.navigate(["/listings", storeID]);
    this.cancel();
  }

  return_listing() {
    let storeID = this.data["store"];
    this.router.navigate(["/listings", storeID]);
    this.cancel();
  }

  dashboard() {
    this.router.navigate(["/dashboard"]);
    this.cancel();
  }

  settings() {
    this.router.navigate(["/account-settings"]);
    this.cancel();
  }

  discovery() {
    this.router.navigate(["/discovery"]);
    this.cancel();
  }

  cancel() {
    this.dialogRef.close();
  }
}
