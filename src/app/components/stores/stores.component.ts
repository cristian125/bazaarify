import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { Router, Routes } from '@angular/router';
import { NotifyService } from 'app/services/notify/notify.service';
import { StoreService } from 'app/services/store/store.service';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { MatDialog } from '@angular/material';
import { NotifyAboutTwofactorComponent } from 'app/components/dialogs/notify-about-twofactor/notify-about-twofactor.component';
import { PaymentComponent } from 'app/components/dialogs/payment/payment.component';
import { MessagingService } from "app/services/messaging.service";
import { SearchService } from "app/services/search/search.service";
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from "angularfire2/storage";
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})

export class StoresComponent implements OnInit {
  message;
  searchResultsFilter: string = 'all';
  stores: {}[];

  constructor(
    private searchService: SearchService,
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    public afAuth: AngularFireAuth,
    private msgService: MessagingService,
    private notify: NotifyService,
    private userService: UserService,
    private storeService: StoreService,
    private router: Router,
    private http: HttpClient,
    public dialog: MatDialog) {
    }

  ngOnInit() {
    this.userService.user.subscribe(data => {
      if (data && data['uid']) {
        let uid = data['uid'];
        let store_keys = [];
        this.db.list(`Users/${uid}/Stores`).snapshotChanges().subscribe(item => {
          item.forEach(element => {
            let key = element.key;
            store_keys.push(key);
          });

          this.db.list('Stores').snapshotChanges().subscribe(item => {
            let stores = [];
            item.forEach(element => {
              var y = element.payload.val();
              let key = element.key;
              if (store_keys.indexOf(key) !== -1) {
                let temp = [];
                temp['key'] = key;
                this.db.list(`Stores/${key}/Listings`).snapshotChanges().subscribe(listings => {
                  temp['cntListings'] = 0;
                  listings.forEach(element => {
                    var y = element.payload.val();
                    if (Object.prototype.toString.call(y["data"]) !== "[object Undefined]") {
                      temp['cntListings']++;
                    }
                  })
                });
      
                temp['name'] = y['Profile']['data']['name'];
                temp['date'] = y['Profile']['launchDate'];
      
                let logo = this.storage.ref(`Store/${element.key}/Profile/store_logo_medium.jpg`);
                logo.getDownloadURL().subscribe(data => {
                  temp['logo'] = data;
                });
      
                stores.push(temp)
              }
            });
            
            this.stores = stores;
          });
        });
      };
    });
    /**
     * Prompts to grab user fcm access & tokens
     */
    this.msgService.getPermission()
    this.msgService.receiveMessage()
    this.message = this.msgService.currentMessage;

    /**
     * Promt to ask user to enable 2F auth
     */
    if (! sessionStorage.getItem('2FpromptWasFired')) {
      let userDataSubscriber = this.userService.userData.valueChanges().subscribe(data => {
        userDataSubscriber.unsubscribe();
        if (! data['twoFactorAuthEnabled']) {
          sessionStorage.setItem('2FpromptWasFired', 'true');
          let dialogRef = this.dialog.open(NotifyAboutTwofactorComponent, {
            width: '600px'
          });
        }
      });
    }

    /**
     * Promt to ask user to enable 2F auth
     */

    if (! sessionStorage.getItem('balancePromptWasFired')) {
      console.log(this.userService.userUid);
      this.db.list(`Users/${this.userService.userUid}/Billings/CurrentBilling`).valueChanges().subscribe(res => {
        let accountStatus = res[0];
        console.log(accountStatus);
        if (accountStatus['balance']) {
          sessionStorage.setItem('balancePromptWasFired', 'true');
          let dialogRef = this.dialog.open(PaymentComponent, {
            width: '600px'
          });
        }
      });
    }
  }

  storeListings(storeID) {
    this.router.navigate(['/listings', storeID]);
  }

  storePage() {
    this.router.navigate(['/storesmanage/create']);
  }
}
