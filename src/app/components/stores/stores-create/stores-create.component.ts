import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from "angularfire2/database";
import { ListingsService } from '../../../services/listings/listings.service';
import { StoreService } from '../../../services/store/store.service';

@Component({
  selector: 'app-stores-create',
  templateUrl: './stores-create.component.html',
  styleUrls: ['./stores-create.component.scss']
})
export class StoresCreateComponent implements OnInit {
  page_no = 1;
  current_section = "general";
  section_flag = [1, 0, 0, 0, 0, 0, 0];
  save_flag = false;
  showSpinner = false;

  provision_templates = {};

  // General Section
  country = [];
  currency = [];
  language_name = [];
  language_code = [];

  general_mature = "false";

  // Page Section
  page_name = "";
  page_description = "";
  page_location = "";
  page_about = "";
  page_website = "";
  page_email = "";
  account_facebook = "";
  account_handle = "";
  page_account = [];
  page_avatarUrl = "";
  page_coverUrl = "";
  editorConfig = {
    editable: true,
    spellcheck: false,
    height: "150px",
    minHeight: "100px",
    placeholder: "Type something. Test the Editor... ヽ(^。^)丿",
    translate: "no"
  };

  // Store Section
  store_store = "off";
  selected_moderators = [];
  available_moderators = [];
  selected_flag = true;
  available_flag = true;

  // Shipping Addresses Section
  shipping_addresses = [];
  new_address = {};
  selected_shipping = 0;

  // Moderation Section
  moderation_disputes = "false";
  disputes_select = "Percentage";
  moderation_description = "";
  moderation_terms = "";
  moderation_lanuages = [];
  selected_languages = [];
  languages_Settings = {
    singleSelection: false,
    idField: "item_id",
    textField: "item_text",
    enableCheckAll: false,
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 5
  };

  // Advanced Section
  advanced_notification = "false";
  advanced_sendTo = "";
  advanced_metadata = "false";
  advanced_bitcoinFee = "Normal";
  advanced_showSeed = false;

  constructor(
    private listingsService: ListingsService,
    private storeService: StoreService,
    private db: AngularFireDatabase
  ) { }

  ngOnInit() {
    this.listingsService.getAllCountries().subscribe(item => {
      let country = [];
      let currency = [];
      let i = 0;
      item.forEach(element => {
        let y = element.payload.val();
        let temp = {};
        let temp1 = {};
        temp['id'] = i; temp1['id'] = i;
        temp['text'] = y["name"]; temp1['text'] = y['currency'];
        country.push(temp); currency.push(temp1);
        i++;
      });

      this.country = country;
      this.currency = currency;
    });

    this.storeService.getAllLanguages().subscribe(item => {
      let language_name = [];
      let language_code = [];
      for (let key in item) {
        language_name.push(item[key]['name']);
        language_code.push(item[key]['code']);
      }

      this.language_name = language_name;
      this.language_code = language_code;
    });

    this.storeService.getProvisonTemplate().subscribe(item => {
      this.provision_templates = item;
      // console.log(this.provision_templates);
    });

    this.new_address['name'] = "";
    this.new_address['company'] = "";
    this.new_address['street'] = "";
    this.new_address['apt'] = "";
    this.new_address['city'] = "";
    this.new_address['state'] = "";
    this.new_address['code'] = "";
    this.new_address['country'] = "";
    this.new_address['notes'] = "";
  }

  previous() {
    if (this.page_no == 1) return;
    this.page_no--;
  }

  next() {
    if (this.page_no == 2) return;
    this.page_no++;
  }

  // Page Section
  onAvatarSelected(event) {
    let fileToUpload = event.target.files[0];
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.page_avatarUrl = event.target.result;
    }

    reader.readAsDataURL(fileToUpload);
  }

  onCoverSelected(event) {
    let fileToUpload = event.target.files[0];
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.page_coverUrl = event.target.result;
    }

    reader.readAsDataURL(fileToUpload);
  }

  onAddAccount() {
    let temp = {};
    temp['facebook'] = "";
    temp['handle'] = "";
    this.page_account.push(temp);
  }

  onDeleteFirstAccount() {
    this.account_facebook = "";
    this.account_handle = "";
  }

  onDeleteAccount(i) {
    this.page_account.splice(i, 1);
  }

  // Store Section
  onBrowserModerators() {
    this.db.object(`SystemSettings/SearchSettings/currnetActiveCluster`).valueChanges().subscribe(item => {
      this.db.object(`SystemSettings/SearchSettings/ActiveClusters`).valueChanges().subscribe(cluster => {
        let moderators = [];
        for (let key in cluster) {
          if (key == item) {
            let parameters = cluster[key]['parameters'];
            let auth_key = cluster[key]['basic-auth'];
            let moderator_url: string = cluster[key]['patterns']['moderators'];
            let peer_url: string = cluster[key]['patterns']['peer'];
            let image_url: string = cluster[key]['patterns']['image'];
            let dns = cluster[key]['dns-endpoint'];
            moderator_url = moderator_url.replace('$dns-endpoint', dns);
            peer_url = peer_url.replace('$dns-endpoint', dns);
            image_url = image_url.replace('$dns-endpoint', dns);

            this.showSpinner = true;
            this.storeService.browseModerators(moderator_url, auth_key).then(res => {
              for (let key in res) {
                let peerId = res[key];
                let temp_url = "";
                temp_url = peer_url.replace('$peerId', peerId);
                temp_url = temp_url.replace('$useCache', parameters['useCache']);
                this.storeService.getModeratorProfile(temp_url, auth_key).then(res => {
                  let moderator = {};
                  moderator['peerId'] = peerId;
                  moderator['name'] = res['name'];
                  moderator['description'] = res['moderatorInfo']['description'];
                  moderator['location'] = res['location'];
                  moderator['language'] = "";
                  moderator['selected'] = false;
                  let language = res['moderatorInfo']['languages'];
                  let index = this.language_code.indexOf(language[0]);
                  if (index !== -1) {
                    moderator['language'] = this.language_name[index];
                  }

                  if (language.length > 1) {
                    moderator['language'] += ", and " + String(language.length - 1)  + " other languages.";
                  }
                  
                  let hash = res['headerHashes']['tiny'];
                  let temp_imageUrl = "";
                  temp_imageUrl = image_url.replace('$imageHash', hash);
                  this.storeService.getModeratorsAvatar(temp_imageUrl, auth_key).then(res => {
                    var arrayBuffer = new Uint8Array(res);
                    const blob = new Blob([arrayBuffer], {type: "image/jpg"});
                    var reader = new FileReader();
                    reader.onload = (event: any) => {
                      moderator['avatar'] = event.target.result;
                    }
          
                    reader.readAsDataURL(blob);
                  }).catch(err => {
                    console.log(err);
                  });
                  moderators.push(moderator);
                }).catch(() => {

                });
              }
              
              this.showSpinner = false;
            }).catch(() => {
                this.showSpinner = false;
              });
          }
        }

        this.available_moderators = moderators;
      });
    });
  }

  onAvailableModerators(i) {
    this.available_moderators[i]['selected'] = !this.available_moderators[i]['selected'];
  }

  onSelectedModerators(i) {
    this.selected_moderators[i]['selected'] = !this.selected_moderators[i]['selected'];
  }

  onFindModerators() {
    
  }

  // Shipping Addresses Section
  onAddAddress() {
    this.save_flag = true;
    let temp = {};
    temp = Object.assign({}, this.new_address);
    this.shipping_addresses.push(temp);
  }

  onRemoveAddress() {
    this.shipping_addresses.splice(this.selected_shipping, 1);
    this.selected_shipping = 0;
  }

  // Advanced Section
  onShowSeed() {
    this.advanced_showSeed = true;
  }

  onSettings() {
    this.current_section = "settings";
  }

  onGeneral() {
    this.current_section = "general";
    this.section_flag = [1, 0, 0, 0, 0, 0, 0];
  }

  onPage() {
    this.current_section = "page";
    this.section_flag = [0, 1, 0, 0, 0, 0, 0];
  }

  onStore() {
    this.current_section = "store";
    this.section_flag = [0, 0, 1, 0, 0, 0, 0];
  }

  onShippingAddresses() {
    this.current_section = "shipping";
    this.section_flag = [0, 0, 0, 1, 0, 0, 0];
  }

  onBlocked() {
    this.current_section = "blocked";
    this.section_flag = [0, 0, 0, 0, 1, 0, 0];
  }

  onModeration() {
    this.current_section = "moderation";
    this.section_flag = [0, 0, 0, 0, 0, 1, 0];
  }

  onAdavanced() {
    this.current_section = "advanced";
    this.section_flag = [0, 0, 0, 0, 0, 0, 1];
  }

  onSave() {
    this.selected_moderators = [];
    this.available_moderators.forEach(element => {
      if (element['selected']) {
        this.selected_moderators.push(element);
      }
    });
  }

  onCancel() {

  }
}
