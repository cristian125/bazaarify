import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, UrlHandlingStrategy } from "@angular/router";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ListingsService } from "../../services/listings/listings.service";
import { finalize } from "rxjs/operators";
import { MatDialog } from "@angular/material";
import { ListingsDeleteComponent } from "../dialogs/listings-delete/listings-delete.component";
import { NotifyService } from "app/services/notify/notify.service";

@Component({
  selector: "app-listings-clone",
  templateUrl: "./listings-clone.component.html",
  styleUrls: ["./listings-clone.component.scss"]
})
export class ListingsCloneComponent implements OnInit {
  storeID: string;
  listingsID: string;
  newID: string;

  editorConfig = {
    editable: true,
    spellcheck: false,
    height: "150px",
    minHeight: "100px",
    placeholder: "Type something. Test the Editor... ヽ(^。^)丿",
    translate: "no"
  };
  page_no = 1;
  save_flag = false;
  required_field = "";
  imageUpload_flag = true;

  //Page1 UI Value
  moderators = [];
  title: string;
  upload_flag = false;
  allImages = [];
  selectedFile = null;
  price = "";
  price_unit: string;
  allCountries = [];
  condition: string;
  allConditions = [];
  contractType: string;
  allTypes = [];
  sku: string;
  tags = [];
  categories = [];
  mature_content: string = "false";
  reviews: string;

  //Page2 UI Value
  description = "";
  shipping_options = [];
  selected_option = {};
  shipping_filter: string;
  cntServices: number;
  destinations_dataName = [];
  destinations_name = [];
  filter_regions = [];
  filter_key: string;
  destination_Settings = {
    singleSelection: false,
    enableCheckAll: false,
    idField: "item_id",
    textField: "item_text",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 5
  };

  //Page3 UI Value
  inventory_flag = 0;
  quantity_flag = 0;
  quantity = "";
  variants = [];
  variant_inventory = [];
  return_policy = "";
  terms_condition = "";
  coupons = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    private http: HttpClient,
    private listingsService: ListingsService,
    private notify: NotifyService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.storeID = params.storeID;
      this.listingsID = params.listingsID;
      this.newID = params.newID;
      this.page_no = params.pageNo;
    });

    this.condition = "New";
    // this.type = "Listing_post_ob_listing";
    this.price_unit = "ALL";

    this.db
      .list(`Stores/${this.storeID}/Profile/data/storeModerators`)
      .snapshotChanges()
      .subscribe(item => {
        item.forEach(element => {
          this.moderators.push(element.payload.val());
        });
      });

    this.listingsService.getAllCountries().subscribe(item => {
      let temp_dataName = [];
      let temp_name = [];
      item.forEach(element => {
        let y = element.payload.val();
        this.allCountries.push(y["code"]);
        temp_dataName.push(y["dataName"]);
        temp_name.push(y["name"]);
      });
      
      this.destinations_dataName = temp_dataName;
      this.destinations_name = temp_name;
      this.allCountries.sort();
    });

    this.listingsService.getAllSettings().subscribe(item => {
      item.forEach(element => {
        if (element.key == "listingConditions") {
          let y: string = element.payload.val();
          this.allConditions = y.split(",");
        } else if (element.key == "listingTypes") {
          let y = element.payload.val();
          const str = JSON.stringify(y);
          JSON.parse(str, (key, value) => {
            if (key !== "") {
              this.allTypes.push(value);
            }
          });
        }
      });
    });

    this.db
      .list(`SystemSettings/ShippingDestinations`)
      .snapshotChanges()
      .subscribe(item => {
        item.forEach(element => {
          let region = {};
          let y = element.payload.val();
          if (element.key !== "ALL") {
            for (let key in y) {
              region["key"] = key;
              let temp = y[key];
              let data = [];
              temp.forEach(element => {
                let index = this.destinations_dataName.indexOf(element);
                if (index !== -1) {
                  data.push(this.destinations_name[index]);
                }
              });
              region["data"] = data;
              this.filter_regions.push(region);
            }
          } else {
            region["key"] = y;
            region["data"] = this.destinations_name;
            this.filter_regions.push(region);
          }
        });

        this.filter_key = this.filter_regions[0].key;
      });

    this.listingsService
      .getListingsData(this.storeID, this.listingsID)
      .subscribe(item => {
        if (this.upload_flag == false) {
          item.forEach(element => {
            let y = element.payload.val();
            switch (element.key) {
              case "data":
                // Page1
                this.title = y["item"]["title"];
                this.price = y["item"]["price"];
                this.condition = y["item"]["condition"];
                let categories = y["item"]["categories"];
                this.categories = [];
                if (
                  Object.prototype.toString.call(categories) ===
                  "[object Array]"
                ) {
                  categories.forEach(element => {
                    let temp = {};
                    temp["displayValue"] = element;
                    this.categories.push(temp);
                  });
                }

                let tags = y["item"]["tags"];
                this.tags = [];
                if (Object.prototype.toString.call(tags) === "[object Array]") {
                  tags.forEach(element => {
                    let temp = {};
                    temp["displayValue"] = element;
                    this.tags.push(temp);
                  });
                }

                // Page2
                this.selected_option = {};
                this.selected_option["name"] = "";
                this.selected_option["regions"] = [];
                this.selected_option["services"] = [];
                this.selected_option["type"] = "";
                this.shipping_options = [];
                if (
                  Object.prototype.toString.call(y["shippingOptions"]) ===
                  "[object Array]"
                ) {
                  let shipping_options = y["shippingOptions"];
                  shipping_options.forEach(element => {
                    let temp = {};
                    temp["name"] = element["name"];
                    temp["type"] = element["type"];
                    temp["regions"] = [];
                    temp["services"] = [];
                    if (
                      Object.prototype.toString.call(element["regions"]) ===
                      "[object Array]"
                    ) {
                      temp["regions"] = element["regions"];
                    }
                    if (
                      Object.prototype.toString.call(element["services"]) ===
                      "[object Array]"
                    ) {
                      temp["services"] = element["services"];
                    }

                    this.shipping_options.push(temp);
                  });
                  this.selected_option = this.shipping_options[0];
                  this.shipping_filter = this.selected_option["name"];
                } else {
                  this.shipping_options.push(this.selected_option);
                  this.shipping_filter = "";
                }

                this.contractType = y["metadata"]["contractType"];
                this.price_unit = y["metadata"]["pricingCurrency"];
                this.selected_option["type"] = y["metadata"]["format"];
                this.mature_content = y["item"]["nsfw"];
                this.description = y["item"]["description"];

                // Page3
                this.quantity = y["item"]["quantity"];
                this.return_policy = y["refundPolicy"];
                this.terms_condition = y["termsAndConditions"];
                let options = y["item"]["options"];
                this.variants = [];
                if (
                  Object.prototype.toString.call(options) === "[object Array]"
                ) {
                  options.forEach(element => {
                    let variant = {};
                    variant["name"] = element["name"];
                    let choices = [];
                    element["variants"].forEach(element => {
                      let temp = {};
                      temp["displayValue"] = element["name"];
                      choices.push(temp);
                    });
                    variant["choices"] = choices;
                    this.variants.push(variant);
                  });
                }

                let skus = [];
                if (
                  Object.prototype.toString.call(y["item"]["skus"]) ===
                  "[object Array]"
                ) {
                  skus = y["item"]["skus"];
                  for (let sku of skus) {
                    let inventory = [];
                    inventory["productID"] = sku["productID"];
                    inventory["quantity"] = sku["quantity"];
                    inventory["surcharge"] = sku["surcharge"];
                    inventory["variantCombo"] = sku["variantCombo"];
                    let data = [];
                    for (let i = 0; i < sku["variantCombo"].length; i++) {
                      data.push(
                        options[i]["variants"][sku["variantCombo"][i]]["name"]
                      );
                    }
                    inventory["data"] = data;
                    this.variant_inventory.push(inventory);
                  }

                  this.quantity_flag = 0;
                  if (this.variant_inventory[0]["quantity"] !== "") {
                    this.inventory_flag = 1;
                  }
                } else {
                  if (this.quantity !== "") {
                    this.quantity_flag = 1;
                    this.inventory_flag = 1;
                  }
                }

                let coupons = y["coupons"];
                this.coupons = [];
                if (
                  Object.prototype.toString.call(coupons) === "[object Array]"
                ) {
                  coupons.forEach(element => {
                    this.coupons.push(element);
                  });
                }

                break;
              case "files":
                this.allImages = [];
                y.forEach(element => {
                  let temp = {};
                  temp["name"] = element;
                  this.listingsService
                    .getListingsMainavatar(
                      this.storeID,
                      this.listingsID,
                      element
                    )
                    .subscribe(data => {
                      temp["url"] = data;
                    });

                  this.allImages.push(temp);
                });
                break;
              case "type":
                // this.type = y;
                break;
            }
          });
        } else {
          item.forEach(element => {
            let y = element.payload.val();
            if (element.key == "files") {
              this.allImages = [];
              y.forEach(element => {
                let temp = {};
                temp["name"] = element;
                this.listingsService
                  .getListingsMainavatar(this.storeID, this.listingsID, element)
                  .subscribe(data => {
                    temp["url"] = data;
                  });

                this.allImages.push(temp);
              });
            }
          });
          this.upload_flag = false;
        }
      });
  }

  previous() {
    if (this.page_no == 1) return;
    this.page_no--;
  }

  next() {
    if (this.page_no == 4) return;
    switch (this.page_no) {
      case 1:
        if (this.onPage1Validation() == false) {
          this.save_flag = true;
          return;
        }
        break;
      case 2:
        if (this.onPage2Validation() == false) {
          this.save_flag = true;
          return;
        }
        break;
      case 3:
        if (this.onPage3Validation() == false) {
          this.save_flag = true;
          return;
        }
        break;
    }

    this.page_no++;
  }

  onFileSelected(event) {
    if (this.imageUpload_flag == true) {
      let i = 0;
      this.allImages.forEach(element => {
        const xhr = new XMLHttpRequest();
        xhr.responseType = "blob";
        xhr.onload = event => {
          const blob = new Blob([xhr.response], { type: "image/jpg" });
          let ref = this.storage.ref(`Store/${this.storeID}/${this.newID}/`);
          let task = ref.child(element.name).put(blob);

          if (i == this.allImages.length - 1) {
            task
              .snapshotChanges()
              .pipe(finalize(() => this.onImageUploadCallback()))
              .subscribe();
          }
          i++;
        };

        xhr.open("GET", element["url"]);
        xhr.send();
      });

      this.imageUpload_flag = false;
    }

    this.selectedFile = event.target.files;

    let ref = this.storage.ref(`Store/${this.storeID}/${this.newID}/`);
    let task = ref.child(this.selectedFile[0].name).put(this.selectedFile[0]);

    let images = [];
    this.allImages.forEach(element => {
      images.push(element["name"]);
    });

    for (let i = 0; i < this.selectedFile.length; i++) {
      let ref = this.storage.ref(`Store/${this.storeID}/${this.listingsID}/`);
      let task = ref.child(this.selectedFile[i].name).put(this.selectedFile[i]);

      images.push(this.selectedFile[i].name);
      if (i == this.selectedFile.length - 1) {
        task
          .snapshotChanges()
          .pipe(
            finalize(() =>
              this.db
                .object(`Stores/${this.storeID}/Listings/${this.newID}/files/`)
                .set(images)
                .then(function() {
                  console.log("Success");
                })
                .catch(function(error) {
                  console.log("Error: " + error);
                })
            )
          )
          .subscribe();
      }
    }
  }

  onImageUploadCallback() {
    this.listingsService
      .getListingsData(this.storeID, this.newID)
      .subscribe(item => {
        item.forEach(element => {
          let y = element.payload.val();
          if (element.key == "files") {
            this.allImages = [];
            y.forEach(element => {
              let temp = {};
              temp["name"] = element;
              this.listingsService
                .getListingsMainavatar(this.storeID, this.newID, element)
                .subscribe(data => {
                  temp["url"] = data;
                });

              this.allImages.push(temp);
            });
          }
        });
      });
  }

  remove_image(i) {
    let ref = this.storage.ref(`Store/${this.storeID}/${this.listingsID}/`);
    ref.child(this.allImages[i]["name"]).delete();

    this.allImages.splice(i, 1);
    let images = [];
    this.allImages.forEach(element => {
      images.push(element["name"]);
    });

    this.upload_flag = true;
    this.db
      .object(`Stores/${this.storeID}/Listings/${this.listingsID}/files/`)
      .set(images)
      .then(function() {
        console.log("Success");
      })
      .catch(function(error) {
        console.log("Error: " + error);
      });
  }

  onAddShipping() {
    let shipping_options = {};
    shipping_options["name"] = "";
    shipping_options["regions"] = [];
    shipping_options["services"] = [];
    shipping_options["type"] = "";

    this.shipping_filter = "";
    this.selected_option = shipping_options;
    this.shipping_options.push(shipping_options);
    this.cntServices = this.selected_option["services"].length;
    console.log(this.shipping_options);
  }

  onDeleteShipping() {
    let flag = false;
    let index = 0;
    let i = 0;
    this.shipping_options.forEach(element => {
      if (element["name"] == this.shipping_filter) {
        flag = true;
        index = i;
      }
      i++;
    });

    if (flag === false) {
      return;
    }

    this.shipping_options.splice(index, 1);
    if (this.shipping_options.length == 0) {
      let shipping_options = {};
      shipping_options["name"] = "";
      shipping_options["regions"] = [];
      shipping_options["services"] = [];
      shipping_options["type"] = "";

      this.shipping_filter = "";
      this.selected_option = shipping_options;
      this.shipping_options.push(shipping_options);
    } else {
      this.selected_option = this.shipping_options[0];
      this.shipping_filter = this.selected_option["name"];
    }
  }

  inputTitle(event) {
    this.shipping_filter = event.target.value;
  }

  onShippingsFilter(value) {
    this.shipping_filter = value;
    this.shipping_options.forEach(element => {
      if (element["name"] == value) {
        this.selected_option = element;
      }
    });
  }

  onRegionsFilter(value) {
    this.filter_regions.forEach(element => {
      if (element.key == value) {
        this.selected_option["regions"] = element.data;
      }
    });
  }

  onAddService() {
    let service = {};
    service["name"] = "";
    service["price"] = "";
    service["estimatedDelivery"] = "";
    service["additionalItemPrice"] = "";

    this.selected_option["services"].push(service);
  }

  onDeleteService(i) {
    console.log("delete service");
    this.selected_option["services"].splice(i, 1);
  }

  onAddVariants() {
    let variant = {};
    variant["name"] = "";
    variant["choices"] = [];

    this.variants.push(variant);
  }

  onDeleteVariants(i) {
    this.variants.splice(i, 1);
    this.onChoicesEvent();
  }

  onAddCoupons() {
    let coupon = {};
    coupon["title"] = "";
    coupon["discountCode"] = "";
    coupon["percentDiscount"] = "";

    this.coupons.push(coupon);
  }

  onDeleteCoupons(i) {
    console.log("del coupons");
    this.coupons.splice(i, 1);
  }

  onInventoryManagement(value) {
    let variants = [];
    this.variants.forEach(element => {
      let temp = element["choices"];

      if (temp.length !== 0) {
        let filter = [];
        temp.forEach(element => {
          filter.push(element["displayValue"]);
        });

        variants.push(filter);
      }
    });

    if (variants.length > 0) {
      this.quantity_flag = 0;
    } else {
      this.quantity_flag = value;
    }
  }

  onChoicesEvent() {
    let variants = [];
    this.variants.forEach(element => {
      let temp = element["choices"];

      if (temp.length !== 0) {
        let filter = [];
        temp.forEach(element => {
          filter.push(element["displayValue"]);
        });

        variants.push(filter);
      }
    });

    if (variants.length > 0) {
      let temp = this.variant_inventory;
      this.variant_inventory = this.getRealArray(variants, variants.length);
      this.quantity_flag = 0;

      for (let element of temp) {
        let temp_data = element["data"];
        for (let inventory of this.variant_inventory) {
          let equal = false;
          let data = inventory["data"];
          if (temp_data.length != data.length) break;
          for (let i = 0; i < temp_data.length; i++) {
            if (temp_data[i] != data[i]) {
              equal = false;
              break;
            }
            equal = true;
          }

          if (equal === true) {
            inventory["productID"] = element["productID"];
            inventory["surcharge"] = element["surcharge"];
            inventory["quantity"] = element["quantity"];
            break;
          }
        }
      }
    } else {
      this.variant_inventory = [];
      this.quantity_flag = this.inventory_flag;
    }
  }

  getRealArray(variants, n) {
    let skus = [];
    let result = [];
    if (n < 2) {
      let ret = [];
      let i = 0;
      if (
        Object.prototype.toString.call(variants[0]["data"]) ===
        "[object Undefined]"
      ) {
        for (let variant of variants[0]) {
          let sku = {};
          let temp = [];
          let no = [];
          temp.push(variant);
          no.push(i);
          sku["data"] = temp;
          sku["variantCombo"] = no;
          sku["productID"] = "";
          sku["surcharge"] = "0";
          sku["quantity"] = "";
          ret.push(sku);
          i++;
        }
        return ret;
      }
      return variants;
    }

    let i = 0,
      j = 0;
    for (let temp1 of variants[n - 2]) {
      for (let temp2 of variants[n - 1]) {
        let ret = {};
        let check = [];
        let no = [];
        check.push(temp1);
        no.push(i);
        if (
          Object.prototype.toString.call(temp2["data"]) === "[object Array]"
        ) {
          for (let temp3 of temp2["data"]) check.push(temp3);
          for (let temp3 of temp2["variantCombo"]) no.push(temp3);
        } else if (
          Object.prototype.toString.call(temp2["data"]) === "[object Undefined]"
        ) {
          check.push(temp2);
          no.push(j);
        } else {
          check.push(temp2["data"]);
          no.push(temp2["variantCombo"]);
        }

        ret["data"] = check;
        ret["variantCombo"] = no;
        ret["productID"] = "";
        ret["surcharge"] = "0";
        ret["quantity"] = "";
        result.push(ret);
        j++;
      }
      i++;
      j = 0;
    }

    variants.pop();
    variants.pop();
    variants.push(result);
    if (variants.length == 1) {
      skus = this.getRealArray(result, variants.length);
    } else {
      skus = this.getRealArray(variants, variants.length);
    }

    return skus;
  }

  unlimited(event, i) {
    if (event.target.checked) {
      this.variant_inventory[i]["quantity"] = "-";
    } else {
      this.variant_inventory[i]["quantity"] = "";
    }
  }

  onSave() {
    this.save_flag = true;
    let validation_flag = this.onCheckValidation();
    if (validation_flag == false) {
      let dialogRef = this.dialog.open(ListingsDeleteComponent, {
        data: {
          store: this.storeID,
          listings: this.listingsID,
          title: "Validation",
          content: this.required_field,
          mode: "validation"
        },
        width: "400px"
      });
      return;
    }

    // upload image
    let images = [];
    if (this.imageUpload_flag == false) {
      this.allImages.forEach(element => {
        images.push(element["name"]);
      });
    }

    // save data
    let categories = [];
    this.categories.forEach(element => {
      categories.push(element["displayValue"]);
    });

    let tags = [];
    this.tags.forEach(element => {
      tags.push(element["displayValue"]);
    });

    let options = [];
    this.variants.forEach(element => {
      let temp = element["choices"];

      if (temp.length !== 0) {
        let option = {};
        option["name"] = element["name"];
        option["description"] = "";
        option["variants"] = [];
        temp.forEach(element => {
          let ret = {};
          ret["name"] = element["displayValue"];
          option["variants"].push(ret);
        });

        options.push(option);
      }
    });

    let skus = [];
    this.variant_inventory.forEach(element => {
      let sku = {};
      sku["variantCombo"] = element["variantCombo"];
      sku["productID"] = element["productID"];
      sku["surcharge"] = element["surcharge"];
      sku["quantity"] = element["quantity"];
      skus.push(sku);
    });

    let listings;
    if (this.imageUpload_flag == true) {
      listings = {
        data: {
          hash: "",
          item: {
            categories: categories,
            tags: tags,
            price: this.price,
            processingTime: "",
            title: this.title,
            quantity: this.quantity,
            description: this.description,
            freeShipping: [],
            language: "",
            nsfw: this.mature_content,
            condition: this.condition,
            options: options,
            skus: skus
          },
          shippingOptions: this.shipping_options,
          taxes: [],
          coupons: this.coupons,
          moderators: this.moderators,
          refundPolicy: this.return_policy,
          termsAndConditions: this.terms_condition,
          metadata: {
            contractType: this.contractType,
            expiry: "2037-12-31T05:00:00.000Z",
            format: this.selected_option["type"],
            pricingCurrency: this.price_unit
          }
        },
        files: images,
        direction: "obfb",
        fbId: this.newID,
        importId: "",
        obId: "",
        storeId: this.storeID,
        topic: "",
        type: "Listing_post_ob_listing"
      };
    } else {
      listings = {
        data: {
          hash: "",
          item: {
            categories: categories,
            tags: tags,
            price: this.price,
            processingTime: "",
            title: this.title,
            quantity: this.quantity,
            description: this.description,
            freeShipping: [],
            language: "",
            nsfw: this.mature_content,
            condition: this.condition,
            options: options,
            skus: skus
          },
          shippingOptions: this.shipping_options,
          taxes: [],
          coupons: this.coupons,
          moderators: this.moderators,
          refundPolicy: this.return_policy,
          termsAndConditions: this.terms_condition,
          metadata: {
            contractType: this.contractType,
            expiry: "2037-12-31T05:00:00.000Z",
            format: this.selected_option["type"],
            pricingCurrency: this.price_unit
          }
        },
        direction: "obfb",
        fbId: this.newID,
        importId: "",
        obId: "",
        storeId: this.storeID,
        topic: "",
        type: "Listing_post_ob_listing"
      };
    }

    if (this.imageUpload_flag == true) {
      let i = 0;
      this.allImages.forEach(element => {
        const xhr = new XMLHttpRequest();
        xhr.responseType = "blob";
        xhr.onload = event => {
          const blob = new Blob([xhr.response], { type: "image/jpg" });
          let ref = this.storage.ref(`Store/${this.storeID}/${this.newID}/`);
          let task = ref.child(element.name).put(blob);

          if (i == this.allImages.length - 1) {
            task
              .snapshotChanges()
              .pipe(finalize(() => this.onReturnListing()))
              .subscribe();
          }
          i++;
        };

        xhr.open("GET", element["url"]);
        xhr.send();
        images.push(element["name"]);
      });
    }

    this.db
      .object(`Stores/${this.storeID}/Listings/${this.newID}`)
      .update(listings)
      .then(function() {
        console.log("Success");
      })
      .catch(function(error) {
        console.log("Error: " + error);
      });

    if (this.imageUpload_flag == false) {
      this.onReturnListing();
    }
    this.imageUpload_flag = false;
    this.notify.success("You clone listing data successfully.");
  }

  onPage1Validation() {
    // Page1
    if (this.allImages.length == 0) {
      this.required_field = ",Image";
      return false;
    }
    if (this.title == "") {
      this.required_field = ",Title";
      return false;
    }
    if (this.price == "") {
      this.required_field = ",Price";
      return false;
    }
    if (this.tags.length == 0) {
      this.required_field = ",Tags";
      return false;
    }
    if (this.categories.length == 0) {
      this.required_field = ",Categories";
      return false;
    }

    return true;
  }

  onPage2Validation() {
    for (let element of this.shipping_options) {
      if (element["regions"].length == 0) {
        this.required_field = "Shipping Options,Countries";
        return false;
      }

      if (element["name"] == "") {
        this.required_field = "Shipping Options,Title";
        console.log(this.required_field);
        return false;
      }

      if (element["type"] == "") {
        this.required_field = "Shipping Options,Type";
        return false;
      }

      let services = element["services"];
      for (let service of services) {
        if (service["name"] == "") {
          this.required_field = "Shipping Options,Service";
          return false;
        }

        if (service["estimatedDelivery"] == "") {
          this.required_field = "Shipping Options,Delivery Time";
          return false;
        }

        if (service["price"] == "") {
          this.required_field = "Shipping Options,Price";
          return false;
        }

        if (service["additionalItemPrice"] == "") {
          this.required_field = "Shipping Options,Additional Price";
          return false;
        }
      }
    }

    return true;
  }

  onPage3Validation() {
    if (this.quantity_flag == 1) {
      if (this.quantity == "") {
        this.required_field = "Inventory Management,Quantity";
        return false;
      }
    }
    
    for (let variant of this.variants) {
      if (variant["name"] == "") {
        this.required_field = "Variants,Title";
        return false;
      }

      let choices = variant["choices"];
      if (choices.length < 2) {
        this.required_field = "Variants,Choices";
        return false;
      }
    }

    for (let inventory of this.variant_inventory) {
      if (this.inventory_flag == 1) {
        if (inventory["quantity"] == "") {
          this.required_field = "Variant Inventory,Quantity";
          return false;
        }
      }
    }

    return true;
  }

  onCheckValidation() {
    // Page1
    if (this.allImages.length == 0) {
      this.required_field = ",Image";
      return false;
    }
    if (this.title == "") {
      this.required_field = ",Title";
      return false;
    }
    if (this.price == "") {
      this.required_field = ",Price";
      return false;
    }
    if (this.tags.length == 0) {
      this.required_field = ",Tags";
      return false;
    }
    if (this.categories.length == 0) {
      this.required_field = ",Categories";
      return false;
    }

    // Page2
    for (let element of this.shipping_options) {
      if (element["regions"].length == 0) {
        this.required_field = "Shipping Options,Countries";
        return false;
      }

      if (element["name"] == "") {
        this.required_field = "Shipping Options,Title";
        console.log(this.required_field);
        return false;
      }

      if (element["type"] == "") {
        this.required_field = "Shipping Options,Type";
        return false;
      }

      let services = element["services"];
      for (let service of services) {
        if (service["name"] == "") {
          this.required_field = "Shipping Options,Service";
          return false;
        }

        if (service["estimatedDelivery"] == "") {
          this.required_field = "Shipping Options,Delivery Time";
          return false;
        }

        if (service["price"] == "") {
          this.required_field = "Shipping Options,Price";
          return false;
        }

        if (service["additionalItemPrice"] == "") {
          this.required_field = "Shipping Options,Additional Price";
          return false;
        }
      }
    }

    // Page3
    if (this.quantity_flag == 1) {
      if (this.quantity == "") {
        this.required_field = "Inventory Management,Quantity";
        return false;
      }
    }

    for (let variant of this.variants) {
      if (variant["name"] == "") {
        this.required_field = "Variants,Title";
        return false;
      }

      let choices = variant["choices"];
      if (choices.length < 2) {
        this.required_field = "Variants,Choices";
        return false;
      }
    }

    for (let inventory of this.variant_inventory) {
      if (this.inventory_flag == 1) {
        if (inventory["quantity"] == "") {
          this.required_field = "Variant Inventory,Quantity";
          return false;
        }
      }
    }

    for (let coupon of this.coupons) {
      if (coupon["title"] == "") {
        this.required_field = "Coupons,Title";
        return false;
      }

      if (coupon["discountCode"] == "") {
        this.required_field = "Coupons,Code";
        return false;
      }

      if (coupon["percentDiscount"] == "") {
        this.required_field = "Coupons,Discount";
        return false;
      }
    }

    return true;
  }

  onReturnListing() {
    this.router.navigate(["/listings", this.storeID]);
  }
}
