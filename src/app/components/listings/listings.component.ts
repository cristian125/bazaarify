import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage"
import { MatDialog, _getOptionScrollPosition } from "@angular/material";
import { ListingsService } from "../../services/listings/listings.service";
import { ListingsDeleteComponent } from "../dialogs/listings-delete/listings-delete.component";
import { Select2OptionData } from 'ng2-select2';

@Component({
  selector: "app-listings",
  templateUrl: "./listings.component.html",
  styleUrls: ["./listings.component.scss"]
})
export class ListingsComponent implements OnInit {
  storeID: string;
  store_name: string;
  store_logo: string;
  store_location = "";
  store_ratings = {};
  stores = [];
  banner_url: string;
  allCategories = [];
  selectedItems = [];
  dropdownSettings = {};
  listItems: Array<string> = [
    "Price low to high",
    "Price hight to low",
    "Title A-Z",
    "Title Z-A"
  ];
  allListings = [];
  tempListings = [];  // for search
  allFollowing = [];
  allFollowers = [];
  str_search = "";
  shippings = [];
  style_flags = [0, 1, 0, 0];
  router_flag = false;
  sort = 0;
  public exampleData: Array<Select2OptionData>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private db: AngularFireDatabase,
    private storage: AngularFireStorage,
    private listingsService: ListingsService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.storeID = params.storeID;
    });

    this.db.list('Stores').snapshotChanges().subscribe(item => {
      let stores = [];
      item.forEach(element => {
        var y = element.payload.val();
        let temp = [];
        temp['key'] = element.key;
        temp['name'] = y['Profile']['data']['name'];
        stores.push(temp);
      });
      
      this.stores = stores;
    });

    this.listingsService.getAllCountries().subscribe(item => {
      let i = 1;
      let shipps = [];
      let temp = {
        id: 0,
        text: '(Any Country)'
      };
      shipps.push(temp);
      item.forEach(element => {
        let y = element.payload.val();
        let temp = {};
        temp['id'] = i;
        temp['text'] = y["name"]; 
        shipps.push(temp);
        i++;
      });

      this.shippings = shipps;
    });

    let logo = this.storage.ref(`Store/${this.storeID}/Profile/store_banner.jpg`);
    logo.getDownloadURL().subscribe(data => {
      this.banner_url = data;
    });

    this.db.list(`Stores/${this.storeID}`).snapshotChanges().subscribe(item => {
      item.forEach(element => {
        let y = element.payload.val();
        switch (element.key) {
          case 'Profile':
            this.store_name = y['data']['name'];
            this.store_location = y['data']['location'];
            break;
          case 'Followers':
            this.allFollowers = y;
            break;
          case 'Following':
            this.allFollowing = y;
            break;
          case 'Ratings':
            this.store_ratings = y;
        }
      });
    });

    let emoticon = this.storage.ref(`Store/${this.storeID}/Profile/store_logo_small.jpg`);
    emoticon.getDownloadURL().subscribe(data => {
      this.store_logo = data;
    });

    this.listingsService.getListings(this.storeID).subscribe(item => {
      let category = [];
      let listings_img = [];
      let index = 0;
      item.forEach(element => {
        var y = element.payload.val();
        let listingsID = element.key;

        if (Object.prototype.toString.call(y["data"]) !== "[object Undefined]") {
          let temp_categories = y["data"]["item"]["categories"];
          temp_categories.forEach(element => {
            if (category.indexOf(element) === -1) {
              category.push(element);
            }
          });
  
          let price = y['data']['item']['price'];
          let file = y["files"][0];
          this.listingsService
            .getListingsMainavatar(this.storeID, listingsID, file)
            .subscribe(data => {
              let temp = [];
              temp["listingsID"] = listingsID;
              temp["file"] = data;
              temp['price'] = price;
              temp['categories'] = y['data']['item']['categories'];
              temp['title'] = y['data']['item']['title'];
              temp['shippingOptions'] = y['data']['shippingOptions'];
              listings_img.push(temp);
              if (index == item.length - 1) {
                this.allListings = listings_img;
                this.tempListings = listings_img;
                this.onSortBy(this.sort);
              }
              index++;
            });
        } else {
          index++;
        }
      });

      this.allCategories = category;
      this.selectedItems = category;
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_id",
      textField: "item_text",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3
    };
  }

  onItemSelect(item: any) {
    this.allListings = this.tempListings;
    let filter_listing = [];
    this.allListings.forEach(element => {
      let categories = element['categories'];
      for (let category of categories) {
        if (this.selectedItems.indexOf(category) !== -1) {
          filter_listing.push(element);
          break;
        }
      }
    });

    this.allListings = filter_listing;
  }

  onSelectAll(items: any) {
    this.allListings = this.tempListings;
    let filter_listing = [];
    this.allListings.forEach(element => {
      let categories = element['categories'];
      for (let category of categories) {
        if (items.indexOf(category) !== -1) {
          filter_listing.push(element);
          break;
        }
      }
    });

    this.allListings = filter_listing;
  }

  onChangedShips(event) {
    let ships = event.data[0]['text'];
    this.allListings = this.tempListings;
    if (ships == '(Any Country)')
      return;

    let filter_listing = [];
    this.allListings.forEach(element => {
      let shippingOptions = element['shippingOptions'];
      for (let option of shippingOptions) {
        if (option['regions'].indexOf(ships) !== -1) {
          filter_listing.push(element);
          break;
        }
      }
    });

    this.allListings = filter_listing;
  }

  onReturnStore() {
    this.router.navigate(['/storesmanage']);
  }

  listings_edit(listingsID) {
    this.router.navigate(["/listings/edit", this.storeID, listingsID, 1]);
  }

  listings_remove(listingsID) {
    this.router_flag = true;
    let dialogRef = this.dialog.open(ListingsDeleteComponent, {
      data: { store: this.storeID, listings: listingsID, 
        title: 'Are you sure?', content: "Once it's deleted, it's gone forever.",
        mode: 'delete_listing'
       },
      width: "300px"
    });
  }

  listings_clone(listingsID) {
    var UUID = require("uuid-js");
    var uuid4 = UUID.create();
    let new_listingsID = uuid4.toString();
    this.router.navigate([
      "/listings/clone",
      this.storeID,
      listingsID,
      new_listingsID,
      1
    ]);
  }

  onSortBy(value) {
    value = Number(value);
    switch (value) {
      case 0:
        // Price low to high
        this.allListings.sort((listing1, listing2) => {
          if (listing1.price > listing2.price) {
            return 1;
          } else if (listing1.price < listing2.price) {
            return -1;
          }

          return 0;
        });
        break;
      case 1:
        // Price high to low
        this.allListings.sort((listing1, listing2) => {
          if (listing1.price < listing2.price) {
            return 1;
          } else if (listing1.price > listing2.price) {
            return -1;
          }

          return 0;
        });
        break;
      case 2:
        // Title A-Z
        this.allListings.sort((listing1, listing2) => {
          if (listing1.title > listing2.title) {
            return 1;
          } else if (listing1.title < listing2.title) {
            return -1;
          }

          return 0;
        });
        break;
      case 3:
        // Title Z-A
        this.allListings.sort((listing1, listing2) => {
          if (listing1.title < listing2.title) {
            return 1;
          } else if (listing1.title > listing2.title) {
            return -1;
          }

          return 0;
        });
        break;
    }
  }

  onSearch(event) {
    let value = event.target.value;
    if (event.key == 'Enter') {
      this.allListings = this.tempListings;
      this.allListings = this.allListings.filter(listing => {
        let title:string = listing.title;
        if (title.toLowerCase().indexOf(value.toLowerCase()) !== -1) {
          return 1;
        }

        return 0;
      });
    }
  }

  onBtnSearch() {
    this.allListings = this.tempListings;
      this.allListings = this.allListings.filter(listing => {
        let title:string = listing.title;
        if (title.toLowerCase().indexOf(this.str_search.toLowerCase()) !== -1) {
          return 1;
        }

        return 0;
      });
  }

  onCreateListing() {
    var UUID = require("uuid-js");
    var uuid4 = UUID.create();
    let new_listingsID = uuid4.toString();
    this.router.navigate(["listings/create/", this.storeID, new_listingsID]);
  }

  onCustomize() {

  }

  onHome() {
    this.style_flags = [0, 0, 0, 0];
    this.style_flags[0] = 1;
  }

  onStore() {
    this.style_flags = [0, 0, 0, 0];
    this.style_flags[1] = 1;
  }

  onFollowing() {
    this.style_flags = [0, 0, 0, 0];
    this.style_flags[2] = 1;
  }

  onFollowers() {
    this.style_flags = [0, 0, 0, 0];
    this.style_flags[3] = 1;
  }

  onListingDisplay(listingsID) {
    if (this.router_flag != true) {
      this.router.navigate(['/listings/display', this.storeID, listingsID]);
    }

    this.router_flag = false;
  }

  onSwitchStores(store_name) {
    let index = 0;
    for (index = 0; index < this.stores.length; index++) {
      if (store_name == this.stores[index]['name'])
        break;
    }

    let storeID = this.stores[index]['key'];
    this.storeID = storeID;
    this.banner_url = "";
    let logo = this.storage.ref(`Store/${this.storeID}/Profile/store_banner.jpg`);
    logo.getDownloadURL().subscribe(data => {
      this.banner_url = data;
    });

    this.store_name = "";
    this.allFollowers = [];
    this.allFollowing = [];
    this.store_location = "";
    this.store_ratings = {};
    this.db.list(`Stores/${this.storeID}`).snapshotChanges().subscribe(item => {
      item.forEach(element => {
        let y = element.payload.val();
        switch (element.key) {
          case 'Profile':
            this.store_name = y['data']['name'];
            this.store_location = y['data']['location'];
            break;
          case 'Followers':
            this.allFollowers = y;
            break;
          case 'Following':
            this.allFollowing = y;
            break;
          case 'Ratings':
            this.store_ratings = y;
        }
      });
    });

    this.store_logo = "";
    let emoticon = this.storage.ref(`Store/${this.storeID}/Profile/store_logo_small.jpg`);
    emoticon.getDownloadURL().subscribe(data => {
      this.store_logo = data;
    });

    this.listingsService.getListings(this.storeID).subscribe(item => {
      let category = [];
      let listings_img = [];
      let index = 0;
      item.forEach(element => {
        var y = element.payload.val();
        let listingsID = element.key;

        if (Object.prototype.toString.call(y["data"]) !== "[object Undefined]") {
          let temp_categories = y["data"]["item"]["categories"];
          temp_categories.forEach(element => {
            if (category.indexOf(element) === -1) {
              category.push(element);
            }
          });
  
          let price = y['data']['item']['price'];
          let file = y["files"][0];
          this.listingsService
            .getListingsMainavatar(this.storeID, listingsID, file)
            .subscribe(data => {
              let temp = [];
              temp["listingsID"] = listingsID;
              temp["file"] = data;
              temp['price'] = price;
              temp['categories'] = y['data']['item']['categories'];
              temp['title'] = y['data']['item']['title'];
              temp['shippingOptions'] = y['data']['shippingOptions'];
              listings_img.push(temp);
              if (index == item.length - 1) {
                this.allListings = listings_img;
                this.tempListings = listings_img;
                this.onSortBy(this.sort);
              }
              index++;
            });
        } else {
          index++;
        }
      });

      this.allCategories = category;
      this.selectedItems = category;
    });
  }
}
